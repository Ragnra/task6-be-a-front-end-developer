import '../styles/index.scss';

$(document).ready(()=>{
    
     $(window).scroll(function () {
  
        var $navbar = $(".navbarTop");
        $navbar.toggleClass('scrolledNav', $(this).scrollTop() > $navbar.height());
        
     });

    $('.accElementTxt').hide();
    $('.accElementTxt:first').show();
    
    $('.accordionBtn').click(function() {
        
        $( '.arrow' ).addClass( "arrowDown" ).removeClass( 'arrowUp' );
        
        $('.accElementTxt').slideUp();

        if ( $(this).next().is(':not(:visible)')){
            $(this).next().slideDown();
            $(this).find( '.arrow' ).toggleClass("arrowUp arrowDown"); 
        } 
      });

      $('.slider').bxSlider({
        auto: true,
        stopAutoOnClick: false,
        pager: false
      });

});
